var JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;
const mongoose = require('mongoose')
const User = mongoose.model('users')
const key = require('./key').secretOrKey

var opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = key
// opts.issuer = 'accounts.examplesoft.com';
// opts.audience = 'yoursite.net';


module.exports = passport => {
    passport.use(new JwtStrategy(opts, function (jwt_payload, done) {
        User.findById(jwt_payload.id)
        .then(user=>{
            if(user){
                return done(null,user)
            }
            return done(null,false)
        }).catch(err=>{
            console.error(err);
        })
    }));

}