const User = require('../models/User')

module.exports = (req,res,next)=>{
    User.findOne({_id:req.user.id}).then(user=>{
        if(user.identity!="manager"){
            return res.json({error:'user identity has no right to do this process'})
        }
        next()
    }).catch(err=>{
        throw err
    })
}