const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ProfileSechma = new Schema({
    type:{
        type:String,
        required:true
    },
    describe:{
        type:String,
        required:true
    },
    income:{
        type:Number,
        required:true
    },
    expend:{
        type:Number,
        required:true
    },
    cash:{
        type:Number,
        required:true
    },
    remark:{
        type:String,
    },
    date:{
        type:Date,
        default:Date.now()
    },
})

module.exports = Profile = mongoose.model('profiles',ProfileSechma)