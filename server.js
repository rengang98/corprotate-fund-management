const express = require('express')
const app = new express()
const mongoose = require('mongoose')
const db = require('./config/key').url
const port = process.env.port || 5000
const users = require('./routers/api/user')
const profiles = require('./routers/api/profile')
const bodyparser = require('body-parser')
const passport = require('passport')

mongoose.connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log('mongoDB connected');
    }).catch(err => {
        console.error(err);
    })

//返回的对象是一个键值对，当extended为false的时候，键值对中的值就为'String'或'Array'形式，为true的时候，则可为任何数据类型。
app.use(bodyparser.urlencoded({extended:false}))

//passport初始化
app.use(passport.initialize())
require('./config/passport')(passport)


app.use('/api/users',users)
app.use('/api/profiles',profiles)

app.listen(port, () => {
    console.log('Server is listening on port:' + port)
})

app.get('/', (req, res, next) => {
    res.send({ data: 'hello' })
})