export const SET_AUTHENTICATED = (state, isAuthenticated) => {
    if (isAuthenticated) state.isAuthenticated = isAuthenticated
    else state.isAuthenticated = false
}
export const SET_USER = (state, user) => {
    if (user) state.user = user
    else user = {}
}
export const LOG_OUT = (state) =>{
    localStorage.removeItem('eleToken')
    SET_AUTHENTICATED(state,false)
    SET_USER(state,{})
}

// export const SET_FUNDDATA = (state,fundData) =>{
//     state.fundData = fundData
// }
