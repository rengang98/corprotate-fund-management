import { fundData } from "./getters"

export const setAuthenticate = ({ commit }, isAuthenticated) => commit('SET_AUTHENTICATED', isAuthenticated)
export const setUser = ({ commit }, user) => commit('SET_USER', user)
export const logOut = ({commit})=> commit('LOG_OUT')
// export const setFundData = ({commit},fundData)=> commit('SET_FUNDDATA',fundData)