import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import * as getters from "./getters"
import * as mutations from "./mutations"
import * as actions from "./actions"

const state = {
  isAuthenticated: false,
  user: {},

}


export default new Vuex.Store({
  state,
  getters: getters,
  mutations: mutations,
  actions: actions
})
