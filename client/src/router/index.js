import Vue from 'vue'
import VueRouter from 'vue-router'
import index from '../views/index'
import Register from '../views/Register'
import Login from '../views/Login'
import Home from "../views/Home";
import Info from "../views/Info";
import FundList from "../views/FundList";
import NotFound from '../views/404'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect:"/index",
  },
  {
    path: '/index',
    name: 'index',
    component: index,
    children:[
      {
        path:'',
        component:Home
      },
      {
        path:'/home',
        component:Home
      },
      {
        path:'/info',
        component:Info
      },
      {
        path:'/fundlist',
        component:FundList
      }
    ]
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path:'*',
    name:"404",
    component:NotFound
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to,from,next)=>{
  const isLogin = localStorage.eleToken?true:false
  if(to.path=='/login'||to.path=='/register'){
    next()
  }else{
    isLogin?next():next('/login')
  }

})

const originalPush = VueRouter.prototype.push
   VueRouter.prototype.push = function push(location) {
   return originalPush.call(this, location).catch(err => err)
}

export default router
