import axios from "axios";
import { Loading,Message } from 'element-ui';
import router from '../src/router/index'

let loading 

function startLoading(){
    loading = Loading.service({
        lock:true,
        text:'กำลังโหลด...',
        background:'rgba(0,0,0,7)'
    });
}

function endLoading(){
    loading.close()
}

//请求拦截

axios.interceptors.request.use(config=>{
    startLoading()
    //token持久化
    if(localStorage.eleToken){
        config.headers.Authorization = localStorage.eleToken
    }
    return config
},error=>{
    return Promise.reject(error)
})

//响应拦截

axios.interceptors.response.use(response=>{
    endLoading()
    return response
},error=>{
    //判断是否状态吗401（token失效）
    const {status} = error.response
    if(status == 401){
        Message.error('tokenหมดอายุแล้วโปรดเข้าสู่ระบบอีกครั้ง')
        //清除token
        localStorage.removeItem('eleToken')
        router.push('/login')
    }
    endLoading()
    return Promise.reject(error)
})

export default axios