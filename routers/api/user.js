const express = require('express')
const router = express.Router()
const User = require('../../models/User')
const bcrypt = require('bcrypt')
const gravatar = require('gravatar')
const validateRegisterInput = require('../../validate/register')
const validateLoginInput = require('../../validate/login')
const jwt = require('jsonwebtoken')
const secret = require('../../config/key').secretOrKey
const passport = require('passport')

/**
 * @url POST api/users/register
 * @description rigester
 * @access public
 */
router.post('/register', (req, res, next) => {
    const requestBody = req.body
    User.findOne({ email: requestBody.email })
        .then(user => {
            if (user) {
                return res.status(400).json({ error: 'email already exist' })
            } else {
                let { errors, isvalid } = validateRegisterInput(requestBody)
                if (!isvalid) {
                    return res.status(404).json({error:Object.values(errors).join(', ')})
                }
                let avatar = "https:" + gravatar.url(requestBody.email, { s: '200', r: 'pg', d: 'mm' })
                const newUser = new User({
                    name: requestBody.name,
                    email: requestBody.email,
                    password: requestBody.password,
                    password2: requestBody.password2,
                    identity: requestBody.identity,
                    avatar: avatar
                })
                bcrypt.genSalt(10, function (err, salt) {
                    bcrypt.hash(newUser.password, salt, function (err, hash) {
                        if (err) return res.json({error:err.message})
                        newUser.password = hash
                        newUser.save().then(user => res.json(user)).catch(err => console.error(err))
                    })
                })
            }
        })
})


/**
 * @url POST api/users/login
 * @description login interface,return token (by jwt passport)
 * @access public
 */
router.post('/login', (req, res, next) => {
    let loginBody = req.body
    let {errors,isValid} = validateLoginInput(loginBody)
    if(!isValid){
        return res.status(404).json({error:Object.values(errors).join(', ')})
    }

    User.findOne({ email: loginBody.email })
        .then(user => {
            if (!user) {
                return res.status(404).json({ error: "email is not exist!" })
            } else {
                bcrypt.compare(loginBody.password, user.password)
                    .then(isMatch => {
                        if (isMatch == true) {
                            jwt.sign({
                                id: user._id,
                                name: user.name,
                                avatar:user.avatar,
                                identity:user.identity
                            }, secret,
                                { expiresIn: 60*60 }
                                , ((err, token) => {
                                    if (err) return res.json({error:err.message})
                                    res.json({
                                        success: true,
                                        token: 'Bearer ' + token
                                    })
                                }
                                )
                            )
                        } else {
                            return res.status(400).json({ error: "password is wrong!" })
                        }
                    })
            }
        })
})

/**
 * @url GET api/users/current
 * @description current user get his/her info after check token successfuly
 * @access private
 */
router.get('/current',passport.authenticate('jwt',{session:false}),(req,res,next)=>{
    res.json({
        id:req.user.id,
        name:req.user.name,
        email:req.user.email,
        identity:req.user.identity
    })
})



/**
 * @url Get api/users/test
 * @description back test interface
 * @access public
 */
router.get('/test', (req, res, next) => {
    res.json({ msg: "test successed" })
})

module.exports = router