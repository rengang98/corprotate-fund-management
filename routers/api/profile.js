const express = require('express')
const router = express.Router()
const passport = require('passport')
const Profile = require('../../models/Profile')
const User = require('../../models/User')
const profile = require('../../validate/profile')
const validateProfileInput = require('../../validate/profile')
const identityCheck = require('../../middleware/identity')
const { session } = require('passport')

/**
 * @url POST api/profiles/
 * @description add profile
 * @access private
 */
router.post('/', passport.authenticate('jwt', { session: false }), identityCheck, (req, res, next) => {
    let porfileBody = req.body
    console.log(porfileBody);
    let { errors, isValid } = validateProfileInput(porfileBody)
    if (!isValid) {
        return res.status(404).json({error:Object.values(errors).join(', ')})
    }
    User.findById(req.user.id)
        .then(user => {
            if (!user) {
                return res.json({ error: 'your User info already not exist, please try to register again.' })
            }
            // if (user.identity != 'manager') {
            //     return res.json({ identity: 'user identity has no right to do this process' })
            // }
            let newProfile = new Profile({
                type: porfileBody.type,
                describe: porfileBody.describe,
                income: porfileBody.income,
                expend: porfileBody.expend,
                cash: porfileBody.cash,
                remark: porfileBody.remark
            })
            newProfile.save().then(result => {
                res.json(result)
            }).catch(err => {
                console.error(err);
                // throw err
            })

        }).catch(err => {
            res.status(404).json({ error: err.message })
        })
})

/**
 * @url POST api/profiles/edit?profile_id=xxx
 * @description edit profile
 * @access private
 */
router.post('/edit', passport.authenticate('jwt', { session: false }), identityCheck, (req, res) => {
    let editBody = req.body
    let { errors, isValid } = validateProfileInput(editBody)
    if (!isValid) {
        return res.json(errors)
    }
    Profile.findOneAndUpdate(
        { _id: req.query.profile_id },
        {$set: editBody},
        { new: true, useFindAndModify: false },
    ).then(newProfile=>{
        res.json(newProfile)
    }).catch(err=>{
        res.json({error:err.message})
    })

})

/**
 * @url DELETE api/profiles?profile_id=xxx
 * @description delete profile by profile_id
 * @access private
 */
router.delete('/',passport.authenticate('jwt',{session:false}),identityCheck,(req,res,next)=>{
    Profile.findOneAndRemove(
        {_id:req.query.profile_id}
    ).then(rlt=>{
        res.json({msg:'delete seccussed'})
    }).catch(err=>{
        res.json({error:err.message})
    })
})

/**
 * @url GET api/profiles/all
 * @description get all profiles
 * @access public
 */
router.get('/all',(req,res,next)=>{
    Profile.find()
    .then(profiles=>{
        res.json(profiles)
    }).catch(err=>{
        res.json(err.message)
    })
})

/**
 * @url GET api/profiles?profile_id=xxx
 * @description get single profile by profile_id
 * @access public
 */
router.get('/',passport.authenticate('jwt',{session:false}),identityCheck,(req,res,next)=>{
    Profile.findOne({id:req.body.profile_id})
    .then(profile=>{
        res.json(profile)
    }).catch(err=>{
        res.json(err.message)
    })
})


/**
 * @url GET /test
 * @description test interface
 * @access public
 */
router.get('/test', (req, res, next) => {
    res.json({ msg: "profile interface test successed" });
})


module.exports = router