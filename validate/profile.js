const isEmpty = require('./isEmpty')
const validator = require('validator')

module.exports = data=>{
    let errors = {}
    // type:porfileBody.type,
    //         describe:porfileBody.describe,
    //         income:porfileBody.income,
    //         expend:porfileBody.expend,
    //         cash:porfileBody.cash,
    //         remark:porfileBody.remark
    data.type = !isEmpty(data.type)?data.type:''
    data.describe = !isEmpty(data.describe)?data.describe:''
    data.income = !isEmpty(data.income)?data.income:''
    data.expend = !isEmpty(data.expend)?data.expend:''
    data.cash = !isEmpty(data.cash)?data.cash:''
    data.remark = !isEmpty(data.remark)?data.remark:''

    if(validator.isEmpty(data.type)){
        errors.type = 'type cannot be empty'
    }
    if(validator.isEmpty(data.describe)){
        errors.describe = 'describe cannot be empty'
    }
    if(validator.isEmpty(data.income)){
        errors.income = 'income cannot be empty'
    }
    if(validator.isEmpty(data.expend)){
        errors.expend = 'expend cannot be empty'
    }
    if(validator.isEmpty(data.cash)){
        errors.cash = 'cash cannot be empty'
    }
    if(!validator.isEmpty(data.income)&&!validator.isNumeric(data.income)){
        errors.income = 'income can only input number!'
    }
    if(!validator.isEmpty(data.expend)&&!validator.isNumeric(data.expend)){
        errors.expend = 'expend can only input number!'
    }
    if(!validator.isEmpty(data.cash)&&!validator.isNumeric(data.cash)){
        errors.cash = 'cash can only input number!'
    }

    return{
        errors,
        isValid:isEmpty(errors)
    }
}