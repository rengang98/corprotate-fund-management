const isEmpty = require('./isEmpty')
const validator = require('validator')

module.exports = validateRegisterInput = (data) =>{
    let errors = {}
    data.name = !isEmpty(data.name)?data.name:''
    data.password = !isEmpty(data.password)?data.password:''
    data.password2 = !isEmpty(data.password2)?data.password2:''
    data.email = !isEmpty(data.email)?data.email:''
    data.identity = !isEmpty(data.identity)?data.identity:''
    
    if(validator.isEmpty(data.name)){
        errors.name="name cannot be empty !"
    }
    if(validator.isEmpty(data.password)){
        errors.password="password cannot be empty !"
    }
    if(validator.isEmpty(data.password2)){
        errors.password2="password2 cannot be empty !"
    }
    if(validator.isEmpty(data.email)){
        errors.email="email cannot be empty !"
    }
    if(validator.isEmpty(data.identity)){
        errors.identity="You must choose your identity!"
    }
    if(!validator.isEmail(data.email)){
        errors.email="email is illegal !"
    }
    if(!validator.isLength(data.name,{min:2,max:30})){
        errors.name = "Length of name should between 2 to 30 !"
    }
    if(!validator.isLength(data.password,{min:6,max:30})){
        errors.password = "Length of password should between 6 to 30 !"
    }
    if(!data.password.match(/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{0,}$/)){
        errors.password = "password should include at least One letter and One number"
    }
    if(!validator.equals(data.password,data.password2)){
        errors.password2 = "password 2 times not same!"
    }
    return{
        errors,
        isvalid:isEmpty(errors)
    }
}
