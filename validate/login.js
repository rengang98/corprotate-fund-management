const validator = require('validator')
const isEmpty = require('./isEmpty')

let errors = {}

module.exports = data =>{

    data.email=!isEmpty(data.email)?data.email:''
    data.password=!isEmpty(data.password)?data.password:''
    console.log(data);
    if(validator.isEmpty(data.email)){
        errors.email='email cannot be empty !'
    }
    if(validator.isEmpty(data.password)){
        errors.password='password cannot be empty !'
    }
    if(!validator.isEmpty(data.email)&&!validator.isEmail(data.email)){
        errors.email='email form is illegal !'
    }
    if(!validator.isLength(data.password,{min:6,max:30})){
        errors.password='Length of password should between 6 to 30 !'
    }

    return{
        errors,
        isValid:isEmpty(errors)
    }
}